/*LABORATORIO- TRABAJO FINAL*/

#include<stdio.h>
#include<conio.h>
#include<string.h>
#include<stdlib.h>

#define IVA1 10.5
#define IVA2 21

                    //DEFINIMOS LAS ESTRUCTURAS DE DATOS 

//ESTRUCTURA PARA LA EMPRESA.
typedef struct  {
    char nombre [30]; // nombre de la empresa.
    char rubro[20]; // rubro en el que se desempe�a.
    char contribuyente[20]; // tipo de contribuyente.
	unsigned long long cuit; // n� de CUIT de la empresa. 
    char nom_responsable[40]; // nombre del due�o o encargado de la empresa.
} datos_empresa;

//ESTRUCTURA PARA LIBRO DE VENTAS.
typedef struct {
	int dia; // D�a de emisi�n de la venta.
	int mes; // Mes de emisi�n de la venta.
	int anio; // A�o de emisi�n de la venta.
	char tipo_fac [1]; //Tipo de factura.
	unsigned long long num_compr; //N�mero de comprobante
	char cliente [30]; //Nombre del cliente.
	unsigned long long cuit; // N�mero de CUIT
	float iva_1; //IVA 10.5.
	float iva_2; //IVA 21.
	float neto; // Neto para Facturas A
    float total; //Total para Facturas B.
} lib_ventas;

//ESTRUCTURA PARA LIBRO DE COMPRAS.
typedef struct {
	int dia; // D�a de emisi�n de la compra.
	int mes; // Mes de emisi�n de la compra.
	int anio; //A�o de emisi�n de la compa.
	unsigned long long num_compr; //N�mero de comprobante
    unsigned long long cuit; // N�mero de CUIT
    char proveedor [30]; // nombre de la empresa proveedora.
    float iva_1; //IVA 10.5.
	float iva_2; //IVA 21.
	float neto; // Neto para Facturas A
    float total; //Total para Facturas B.
} lib_compras;


                    //PROTOTIPOS DE FUNCIONES
                    
void continuar();

void cargar_empresa();
void mostrar_empresa();
void rotulo(); 

void cargar_ventas();
void mostrar_ventas();

void cargar_compras();
void mostrar_compras();


                   //FUNCI�N PRINCIPAL
                   
int main(){
	system ("cls");
    int opcion=0;
    do {
        printf("\t********************************************************\n");
	    printf("\t                     MENU DE OPCIONES          	      \n");
	    printf("\t********************************************************\n\n");
	    printf("MI EMPRESA\n");
		printf("1- Ingresar/Modificar datos de la empresa\n");
		printf("2- Mostrar datos de la empresa\n\n");
		printf("LIBRO DE VENTAS\n");
        printf("3- Cargar\n");
        printf("4- Mostrar\n\n");
        printf("LIBRO DE COMPRAS\n");
        printf("5- Cargar\n");
        printf("6- Mostrar\n\n");
        
        printf("7- SALIR\n\n");
        printf("OPCION: "); scanf("%i",&opcion);
        switch (opcion) {
            case 1: cargar_empresa();
                   break;
            case 2: mostrar_empresa();
                   break;
            case 3: cargar_ventas();
                   break;
            case 4:mostrar_ventas();
                   break;
            case 5:cargar_compras();
                   break;
            case 6:mostrar_compras();
                   break;
        }
    } while (opcion!=7);
    return 0;
    system ("cls");
}

                    //FUNCIONES
void continuar(){
    printf("Presione una tecla para continuar\n\n");
    getch();
}

void cargar_empresa(){ //CARGAR DATOS DE LA EMPRESA
    system ("cls");
	FILE *arch;
    arch=fopen("datosempresa.dat","wb"); // "wb" siempre genera un nuevo archivo.
    if (arch==NULL){
        exit(1);
	}
	char continua;
    int opcion;
    datos_empresa empresa; // estructura y variable local.
	do{	
        system ("cls");
		printf("Ingrese el nombre de la empresa: \n");
        fflush(stdin);gets(empresa.nombre); system ("cls");
        printf("Ingrese el rubro de la empresa: \n");
        fflush(stdin);gets(empresa.rubro); system ("cls");
        printf("Ingrese el CUIT de la empresa: \n");
        fflush(stdin); scanf("%lli",&empresa.cuit);system ("cls");
        printf("Indique Condici%cn frente al IVA: \n",162);
        printf("1- Responsable Inscripto\n");
        printf("2- Monotributista\n");
        fflush(stdin);scanf("%i",&opcion);
        if (opcion == 1){
        	strcpy(empresa.contribuyente,"RESPONSABLE INSCRIPTO");
		}
		if(opcion==2){
			strcpy(empresa.contribuyente,"MONOTRIBUTISTA");
		}
		system ("cls");
        printf("Ingrese el nombre de %cl/la encargado/a de la empresa: \n",130);
        fflush(stdin);gets(empresa.nom_responsable);
		    	
	    printf("LOS DATOS INGRESADOS SON: \n");
        printf("- Nombre de la empresa: %s\n",empresa.nombre);
        printf("- Rubro: %s\n",empresa.rubro);
        printf("- CUIT: %lli\n",empresa.cuit);
        printf("- Condici%cn frente al IVA: %s\n",162,empresa.contribuyente);
        printf("- Encargado: %s\n\n",empresa.nom_responsable);
        	
        printf("%cLos datos ingresados son correctos%c S/N\n",168,63);
        fflush(stdin); scanf("%c",&continua);system ("cls");
    }while(continua!='s' && continua!='S');
    fwrite(&empresa, sizeof(datos_empresa), 1, arch);system ("cls");
    
    fclose(arch);
    continuar();
    system ("cls");
}

void mostrar_empresa(){ //MUESTRA DATOS DE LA EMPRESA
	system ("cls");
    FILE *arch;
    arch=fopen("datosempresa.dat","rb");
    if (arch==NULL){
    	printf("No se han cargado los datos de la empresa.\n");
        exit(1);
	}
    datos_empresa empresa;
    fread(&empresa, sizeof(datos_empresa), 1, arch);
    printf("Nombre\t\t\tRubro\t\t\tContribuyente\t\t\tCUIT\t\tResponsable\n");
    while(!feof(arch)){
        printf("%s\t\t%s\t\t%s\t\t%lli\t%s\n", empresa.nombre,empresa.rubro,empresa.contribuyente,empresa.cuit,empresa.nom_responsable);
        fread(&empresa, sizeof(datos_empresa), 1, arch);
    }
    fclose(arch);
    continuar();
    system ("cls");
}

void rotulo(){ //Funci�n r�tulo para mostrar los libros.
	FILE *arch;
	arch=fopen("datosempresa.dat","rb");
    if (arch==NULL){
    	printf("No se han cargado los datos de la empresa.\n");
        exit(1);
	}
    datos_empresa empresa;
    fread(&empresa, sizeof(datos_empresa), 1, arch);
    printf("EMPRESA: %s\n",empresa.nombre);
    printf("C.U.I.T.: %lli\n",empresa.cuit);
    printf("CONDICION IVA: %s\n",empresa.contribuyente);
    fclose(arch);
}

void cargar_ventas(){ //CARGAR LIBRO DE VENTAS
    system ("cls");
	FILE *arch;
    arch=fopen("LibroVentas.dat","ab");
    if (arch==NULL){
    	arch=fopen("LibroVentas.dat","wb");
        exit(1);
	}
	char continua;
    int op1, op,existe=0;
    float aux;
    lib_ventas ventas;
	do{ 
	    printf("Ingrese fecha de emisi%cn de la Factura: \n",162);
	    do{
	        printf("D%ca: \n",161); fflush(stdin); scanf("%i",&ventas.dia);
	    	if (ventas.dia>0 && ventas.dia<=31){
	    	    ventas.dia=ventas.dia;
	    	    existe=0;
		   } else{
			    printf("\tERROR. Ingrese nuevamente\n\n");
			    existe=1;
		    }
		}while(existe==1);
		
	    do{
	        printf("Mes: \n"); fflush(stdin); scanf("%i",&ventas.mes);
	    	if (ventas.mes>0 && ventas.mes<=12){
	    	    ventas.mes=ventas.mes;
	    	    existe=0;
		   } else{
			    printf("\tERROR. Ingrese nuevamente\n\n");
			    existe=1;
		    }
		}while(existe==1);
	    
	    printf("A%co: \n",164); fflush(stdin); scanf("%i",&ventas.anio); system ("cls");
		printf("Indique Condici%cn frente al IVA: \n",162);
        printf("1- Responsable Inscripto\n");
        printf("2- Monotributista\n");
        printf("3- Exento\n");
        printf("4- Consumidor Final\n");
        fflush(stdin);scanf("%i",&op1);
		system ("cls");
		if (op1 == 1 || op1==2){
			strcpy(ventas.tipo_fac,"A");
			
		    printf("Ingrese el monto NETO generado: \n");
	        fflush(stdin); scanf("%f",&ventas.neto); system ("cls");
			
	    	printf("Seleccione el IVA que corresponde: \n");
	    	printf("1- 10,5%c\n",37);
	    	printf("2- 21%c\n",37);
	    	printf("3- Ambos\n");
	    	fflush(stdin); scanf("%i",&op);
			if (op==1){
				ventas.iva_1 = ventas.neto * 0.105;
				ventas.iva_2 = 0;
			}
			if(op==2){
				ventas.iva_1 = 0;
				ventas.iva_2 = ventas.neto * 0.21;
			}
			if (op==3){
				ventas.iva_1 = ventas.neto * 0.105;
				ventas.iva_2 = ventas.neto * 0.21;
			}
	        ventas.total= ventas.iva_1 + ventas.iva_2 + ventas.neto;
		}
		system ("cls");
        if (op1 ==3  || op1==4){
        	strcpy(ventas.tipo_fac,"B");
        	
        	printf("Ingrese el monto TOTAL generado: \n");
	        fflush(stdin); scanf("%f",&ventas.total); system ("cls");
        	
	    	printf("Seleccione el IVA que corresponde: \n");
	        printf("1- 10.5%c\n",37);
	        printf("2- 21%c\n",37);
	        fflush(stdin);scanf("%i",&op); 
	        if (op==1){
	        	ventas.iva_1 = ventas.total * 0.105;
	        	ventas.iva_2=0;
	        	ventas.neto= ventas.total - ventas.iva_1;
		    }
		    if (op==2){
		    	ventas.iva_1=0;
				ventas.iva_2 = ventas.total * 0.21;
				ventas.neto= ventas.total - ventas.iva_2;
			}
			system ("cls");	 
	    }
        printf("Ingrese el N%c de Comprobante sin los d%cgitos de empresa: \n",167,161); fflush(stdin); scanf("%lli",&ventas.num_compr);system ("cls");
		
		if (op1 == 1 || op1==2 || op1==3){
        	printf("Ingrese el nombre del cliente: \n"); fflush(stdin); gets(ventas.cliente);system ("cls");
			}
        if(op1==4 && ventas.total>10000){
        	printf("Ingrese el nombre del cliente: \n"); fflush(stdin); gets(ventas.cliente);system ("cls");	
		}
		if(op1==4 && ventas.total<=10000){
        	strcpy(ventas.cliente,"-----------");
		}
		
		if (op1 == 1 || op1==2 || op1==3){
        	printf("Ingrese EL CUIT: \n"); fflush(stdin); scanf("%lli",&ventas.cuit);system ("cls");
		} else{
			ventas.cuit=0;
		}
		system ("cls");
		    	
	    printf("LOS DATOS INGRESADOS SON: \n");
        printf("- Fecha de emisi%cn de la Factura: %i/%i/%i \n",162,ventas.dia,ventas.mes,ventas.anio);
        printf("- Tipo de Factura: %s\n",ventas.tipo_fac);
        printf("- Comprobante N%c: 0001-%lli\n",167, ventas.num_compr);
        printf("- Cliente: %s\n", ventas.cliente);
        if(ventas.cuit!=0){
        	printf("- CUIT: %lli\n",ventas.cuit);
		}else{
			printf("- CUIT: NO APLICA\n");
		}
        printf("- Neto: %c%.2f\n",36,ventas.neto);
        printf("- IVA 10,5%c: %c%.1f\n",37,36,ventas.iva_1);
        printf("- IVA 21%c: %c%.1f\n",37,36,ventas.iva_2);
        printf("- Total: %c%.2f\n",36,ventas.total);
        	
        printf("%cLos datos ingresados son correctos%c S/N\n",168,63);
        fflush(stdin); scanf("%c",&continua);
    }while(continua!='s' && continua!='S');
    
    fwrite(&ventas, sizeof(lib_ventas), 1, arch);system ("cls");
    
    fclose(arch);
    continuar();
    system ("cls");
}

void mostrar_ventas(){ //MUESTRA DATOS DEL LIBRO DE VENTAS
    system("cls");
	FILE *arch;
    arch=fopen("LibroVentas.dat","rb");
    if (arch==NULL){
    	printf("*** ESTE LIBRO ESTA VACIO ***\n");
    	exit(1);
	}
    int mes,anio;
	float neto=0,iva1=0,iva2=0,total=0;
    
    printf("Indique el mes y a%co que desea consultar: \n",164);
    int existe=0;
	do{
	    printf("Mes: \n"); fflush(stdin); scanf("%i",&mes);
	    if (mes>0 && mes<=12){
	    	mes=mes;
	    	existe=0;
		}else{
			printf("\tERROR. Ingrese nuevamente\n\n");
			existe=1;
		}
	}while(existe==1);  
	  
	printf("A%co: \n",164); fflush(stdin); scanf("%i",&anio);
	system("cls");
	
	lib_ventas ventas;
    fread(&ventas,sizeof(lib_ventas),1,arch);
    if (mes==ventas.mes && anio==ventas.anio){
    	printf("\n\t\t\t\t\t%c%c%c LIBRO DE VENTAS %c%c%c\n\n",4,4,4,4,4,4);
	    rotulo(); //Funci�n R�tulo.
	    printf("Mes/A%co: %i/%i\n\n",164, mes, anio);
        printf("\nFECHA\t\tTIPO\tN%c COMPROBANTE\tCLIENTE\t\tCUIT\t\tNETO\t\tIVA 10.5%c\tIVA 21%c\t\tTOTAL\n",167,37,37);
        printf("_____________________________________________________________________________________________________________________________\n");
    	while(!feof(arch)){
		    printf("%i/%i/%i\t%s\t0001-%lli\t%s\t%lli\t%c%.2f\t%c%.1f\t\t%c%.1f\t\t%c%.2f\n",ventas.dia,ventas.mes,ventas.anio,ventas.tipo_fac,ventas.num_compr,ventas.cliente,
		    ventas.cuit,36,ventas.neto,36,ventas.iva_1,36,ventas.iva_2,36,ventas.total);
		    neto= neto + ventas.neto; // acumuladores
		    iva1= iva1 + ventas.iva_1;
		    iva2= iva2 + ventas.iva_2;	
		    total= total + ventas.total;
            fread(&ventas, sizeof(lib_ventas), 1, arch);
	    }
	    printf("_____________________________________________________________________________________________________________________________\n\n");
	    printf("NETO: %c%.2f\n",36,neto);
	    printf("IVA 10,5%c: %c%.1f\n",37,36,iva1);
	    printf("IVA 21%c: %c%.1f\n",37,36,iva2);
	    printf("TOTAL: %c%.2f\n\n",36,total);
    }else{
    	printf("*** NO EXITEN REGISTROS EN ESTA FECHA ***\n");
	}
    fclose(arch);
    continuar();
	system("cls");	
}

void cargar_compras(){ //CARGAR LIBRO DE COMPRAS
    system ("cls");
    char continua;
    int op,existe=0;
	FILE *arch;
    arch=fopen("LibroCompras.dat","ab");
    if (arch==NULL){
    	arch=fopen("LibroCompras.dat","wb");
        exit(1);
	}
	lib_compras compras;
	do{
	    printf("Ingrese fecha de emisi%cn de la Factura: \n",162);
	    do{
	        printf("D%ca: \n",161); fflush(stdin); scanf("%i",&compras.dia);
	    	if (compras.dia>0 && compras.dia<=31){
	    	    compras.dia=compras.dia;
	    	    existe=0;
		   } else{
			    printf("\tERROR. Ingrese nuevamente\n\n");
			    existe=1;
		    }
		}while(existe==1);
	    
	    do{
	        printf("Mes: \n"); fflush(stdin); scanf("%i",&compras.mes);
	    	if (compras.mes>0 && compras.mes<=12){
	    	    compras.mes=compras.mes;
	    	    existe=0;
		   } else{
			    printf("\tERROR. Ingrese nuevamente\n\n");
			    existe=1;
		    }
		}while(existe==1);
	    
	    printf("A%co: \n",164); fflush(stdin); scanf("%i",&compras.anio); system("cls");
	    printf("Ingrese el monto NETO generado: \n"); fflush(stdin); scanf("%f",&compras.neto);system ("cls");
	    printf("Seleccione el IVA que corresponde: \n");
	    printf("1- 10,5%c\n",37);
	    printf("2- 21%c\n",37);
	    printf("3- Ambos\n");
	    fflush(stdin); scanf("%i",&op);
		if (op==1){
			compras.iva_1 = compras.neto * 0.105;
			compras.iva_2 = 0;
		}
		if(op==2){
			compras.iva_1 = 0;
			compras.iva_2 = compras.neto * 0.21;
		}
		if (op==3){
			compras.iva_1 = compras.neto * 0.105;
			compras.iva_2 = compras.neto * 0.21;	
		}
	    compras.total= compras.iva_1 + compras.iva_2 + compras.neto;
        system ("cls");
        
        printf("Ingrese el N%c de Comprobante sin los d%cgitos de empresa: \n",167,161); fflush(stdin); scanf("%lli",&compras.num_compr);system ("cls");
		printf("Ingrese el nombre del proveedor: \n"); fflush(stdin); gets(compras.proveedor);system ("cls");
		printf("Ingrese EL CUIT: \n"); fflush(stdin); scanf("%lli",&compras.cuit);system ("cls");
		system ("cls");
		    	
	    printf("LOS DATOS INGRESADOS SON: \n");
        printf("- Fecha de emisi%cn de la Factura: %i/%i/%i \n",162,compras.dia,compras.mes,compras.anio);
        printf("- Tipo de Factura: A\n");
        printf("- Comprobante N%c: 0001-%lli\n",167, compras.num_compr);
        printf("- Proveedor: %s\n", compras.proveedor);
        printf("- CUIT: %lli\n",compras.cuit);
        printf("- Neto: %c%.2f\n",36,compras.neto);
        printf("- IVA 10,5%c: %c%.1f\n",37,36,compras.iva_1);
        printf("- IVA 21%c: %c%.1f\n",37,36,compras.iva_2);
        printf("- Total: %c%.2f\n",36,compras.total);
        	
        printf("%cLos datos ingresados son correctos%c S/N\n",168,63);
        fflush(stdin); scanf("%c",&continua);
    }while(continua!='s' && continua!='S');
    fwrite(&compras, sizeof(lib_compras), 1, arch);system ("cls");
    
    fclose(arch);
    continuar();
    system ("cls");
}

void mostrar_compras(){ //MUESTRA DATOS DEL LIBRO DE COMPRAS
	system("cls");
	FILE *arch;
    arch=fopen("LibroCompras.dat","rb");
    if (arch==NULL){
    	printf("*** ESTE LIBRO ESTA VACIO ***\n");
    	exit(1);
	}
    char tip_fac; //Variable local Tipo de factura
	int mes,anio;
	float neto=0,iva1=0,iva2=0,total=0; // Acumuladores
    
    printf("Indique el mes y a%co que desea consultar: \n",164);
    int existe=0;
	do{
	    printf("Mes: \n"); fflush(stdin); scanf("%i",&mes);
	    if (mes>0 && mes<=12){
	    	mes=mes;
	    	existe=0;
		}else{
			printf("\tERROR. Ingrese nuevamente\n\n");
			existe=1;
		}
	}while(existe==1);    
	printf("A%co: \n",164); fflush(stdin); scanf("%i",&anio);
	system("cls");
	
	lib_compras compras;
	tip_fac= 'A'; //Tipo de factura = 'A'
	printf("\n\t\t\t\t\t%c%c%c LIBRO DE COMPRAS %c%c%c\n\n",4,4,4,4,4,4);
	rotulo(); //Funci�n R�tulo.
	printf("Mes/A%co: %i/%i\n",164, mes, anio);
    printf("\nFECHA\t\tTIPO\tN%c COMPROBANTE\tCLIENTE\t\tCUIT\t\tNETO\t\tIVA 10.5%c\tIVA 21%c\t\tTOTAL\n",167,37,37);
    printf("_____________________________________________________________________________________________________________________________\n");
    fread(&compras,sizeof(lib_compras),1,arch);
    if (mes==compras.mes && anio==compras.anio){
    	while(!feof(arch)){
		printf("%i/%i/%i\t%c\t0001-%lli\t%s\t%lli\t%c%.2f\t%c%.1f\t\t%c%.1f\t\t%c%.2f\n",compras.dia,compras.mes,compras.anio,tip_fac,compras.num_compr,compras.proveedor,
		compras.cuit,36,compras.neto,36,compras.iva_1,36,compras.iva_2,36,compras.total);

		neto= neto + compras.neto; 
		iva1= iva1 + compras.iva_1; 
		iva2= iva2 + compras.iva_2;	
		total= total + compras.total;
        fread(&compras, sizeof(lib_compras), 1, arch);
        }
	    printf("_____________________________________________________________________________________________________________________________\n\n");
	    printf("NETO: %c%.2f\n",36,neto);
	    printf("IVA 10,5%c: %c%.1f\n",37,36,iva1);
	    printf("IVA 21%c: %c%.1f\n",37,36,iva2);
	    printf("TOTAL: %c%.2f\n",36,total);	
	}else {
		printf("*** NO EXITEN REGISTROS EN ESTA FECHA ***\n");
	}
    fclose(arch);
    continuar();
	system("cls");
}






